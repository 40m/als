function G = sys2TF(sys,f)
% sys2TF takes a Matlab LTI object and returns a complex TF vector
% G = sys3TF(sys,f)
%
% G is the complex transfer function for sys evaluated at the frequency
%   vector 'f' in Hz.
%
% copied from Peter Fritschel, circa 1998


w = 2*pi*f;
[Gm,Gp] = bode(sys,w);

Gm = squeeze(Gm);
Gp = squeeze(Gp);
G = Gm.*exp(1i*Gp*pi/180);
end
