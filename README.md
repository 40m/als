# Arm Length Stabilization (ALS)
[![pipeline status](https://git.ligo.org/40m/als/badges/master/pipeline.svg)](https://git.ligo.org/40m/als/commits/master)

Repo to collect all important information, circuit schematics, controls schematics, transfer function measurements, and noise budget for ALS at 40m.

## Cloning instructions
There are submodules in this repo, so use following options during cloning to
ensure you get all submodules as well:
With version 1.6.5 of Git and later:
```
# Clone with SSH
git clone --recursive git@git.ligo.org:40m/als.git
# Clone with HTTPS
git clone --recursive https://git.ligo.org/40m/als.git
```
For already cloned repo or older versions of Git:
```
# Clone with SSH
git clone git@git.ligo.org:40m/als.git
# Clone with HTTPS
git clone https://git.ligo.org/40m/als.git
cd labutils
git submodule update --init --recursive
```

## Installation
To use code in this repo, you need MATLAB installed with 'Simulink' and 'Control System Toolbox'.
* Run following to create als40m conda environment:
```
conda env create --file als40m.yml
```
* Activate als40m environment and create ipython kernel with it:
```
conda activate als40m
python -m ipykernel install --user --name=als40m
```

* Install matlab.engine (remaining inside als40m conda env):
  * [Source: Getting started with matlab engine API for Python](https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html)
  * matlabroot is the directory where Matlab is installed. On OSX, it is often "/Applications/MATLAB_R20xxa.app".
```
cd "matlabroot/extern/engines/python"
python setup.py install
```

* While opening jupyter notebooks, change kernel to als40m by dropdown menu: Kernel>Change Kernel>als40m

## Noise Budget Plot

![ALS_nb](/noiseBudget/ALS_nb.png)

## Resources

### External links:
* [Advanced LIGO Lock Acquisition Scheme explained](https://docs.google.com/spreadsheets/d/1W5a57PzKdqJDEX7qgSIHhWABiOzwfHKDmFxo4ObBLbI/edit?usp=sharing)

### Elog posts:

##### General ALS:
* [10/6/19: Arm control using error signals achieved ](https://nodus.ligo.caltech.edu:8081/40m/14944)
* [05/26/20: Lock acquisition sequence](https://nodus.ligo.caltech.edu:8081/40m/15349)

##### AM/PM Measurements of AUX Laser:
* [Measurements by Shruti](https://nodus.ligo.caltech.edu:8081/40m/15206)
* [Measurements by Eric](https://nodus.ligo.caltech.edu:8081/40m/12077)

### Documentation about ALS

* [Alexa Staley's thesis](https://doi.org/10.7916/D8X34WQ4)
* [Achieving resonance in the Advanced LIGO gravitational-wave interferometer](https://doi.org/10.1088/0264-9381/31/24/245010)
* [Kiwamu Izumi's thesis](https://dcc.ligo.org/LIGO-P1300001/public)
* [Multi-color Cavity Metrology (2012)](https://doi.org/10.1364/JOSAA.29.002092): Proof-of-principle experiment HIFO at Caltech 40m.
* [Arm-length stabilisation for interferometric gravitational-wave detectors using frequency-doubled auxiliary lasers (2012)](https://doi.org/10.1364/OE.20.000081): Proof-of-principle experiment at ANU with 1.3m suspended cavity.

### Interesting/relevant papers

* [Extending PZT BW by cancelling Mechanical Resonances by IIR on FPGA (2020)](https://doi.org/10.1364/OE.20.000081)
