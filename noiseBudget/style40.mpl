##
## This file contains the matplotlib style settings for generating
## noise requirement figures with normal colors.
##
## For a description of the settings and list of available options see:
## https://matplotlib.org/users/customizing.html#a-sample-matplotlibrc-file
##

## Figure canvas settings ##
figure.facecolor:       white
figure.edgecolor:       white

## Plot axes settings ##
axes.labelsize:         20
axes.facecolor:         white
axes.edgecolor:         black
axes.labelcolor:        black
axes.labelweight:       normal
axes.labelpad:          6
axes.linewidth:         1
axes.grid:              True  
axes.grid.axis:         both      
axes.grid.which:        both     

## Text settings ##
font.size:              20  
text.color:             black 
text.usetex:            False         
# [The below settings have effect only if usetex=False]
font.weight:            normal
font.family:            serif
mathtext.fontset:       dejavuserif

## X-tick settings ##
xtick.labelsize:        20 
xtick.color:            black       
xtick.top:              True         
xtick.bottom:           True    
xtick.direction:        in 
xtick.major.size:       8
xtick.major.width:      1
xtick.major.pad:        5
xtick.minor.size:       4
xtick.minor.width:      1
xtick.minor.visible:    True

## Y-tick settings ##
ytick.labelsize:        20 
ytick.color:            black  
ytick.left:             True      
ytick.right:            True
ytick.direction:        in
ytick.major.size:       8
ytick.major.width:      1
ytick.major.pad:        3.5
ytick.minor.size:       4
ytick.minor.width:      1
ytick.minor.visible:    True

## Grid line settings ##
grid.color:             0.8         
grid.linestyle:         -    
grid.linewidth:         0.5      

## Plot line settings ##
lines.linewidth:        2