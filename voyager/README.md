# Voyager ALS

Relevant stuff for ALS voyager schemes.

### Noise budget comparison of selected schemes
<img src="vALS_Sel_Sch_Comp.png" alt="Noise Budget Comparison" width="800"/>

We are probably going to go with one of the FHG schemes or SROPO Ib. They all have similar performance.

### Schemes with injection at PR3
<img src="schemes/ALSwithFHG_b.svg" alt="FHG_Ib" width="400"/><img src="schemes/ALSwithSROPO_b.svg" alt="SROPO_b" width="400"/>

Inject auxiliary frequency from the beam splitter side. Can convinienty use the IR 1064 nm seed laser pick-offs to take the beatnote.

### FHG Schemes
<img src="schemes/ALSwithFHG.svg" alt="FHG_I" width="400"/> <img src="schemes/ALSwithFHG2.svg" alt="FHG_II" width="400"/>

Do third harmonic generation with cascaded Second Harmonic Generation (SHG) and Sum Frequency Generation (SFG). Then do a Degenerate Parametric Down conversion (DPDC) to get 1.5 times frequency of PSL for AUX. For comparing beatnote, it can be done either at AUX frequency (Scheme II) or 3 times PSL frequency (Scheme I).

#### Findings:
Will it hurt to have PDC w/OPO? Looks like it adds (at least) an additional ctrl loop, and according to [P2000265](https://dcc.ligo.org/DocDB/0169/P2000265/001/main.pdf),
the PDC light retains stability and noise of its pump.

#### Open questions:
- [ ] What is the range of conversion efficiency down the HHG chain? Does it matter?
- [ ] How much amplitude noise would be injected by OPO? What effect does it have on beatnote frequency noise?

### SROPO Schemes

<img src="schemes/SROPO.svg" alt="SROPO_Idea" width="800"/>

If 1064 nm seed laser is first frequency doubled into 532 nm green light and
then this is used to pump a crystal whose edges are curved and coated with
HR coatings to create a cavity for 810 nm, then 532 nm photon will
downconvert into a resonant signal photon at 810nm and a non-resonant idler
photon at 1550 nm. This would be a Singly Resonent Optical parametric
Oscillator (SROPO). The frequency noise of the idler beam would be
quadrature sum of frequency noise of the pump beam and length noise of
crystal. For a etalon cavity, this length noise would be low in high
frequencies and can be suppressed at low frequencies with temperature
control. for high enough finesse of the 810 nm cavity, we can create a pump
noise limited frequency conversion into 1550 nm source.

Similar range of frequency conversion was performed with SROPO achieving
less than 3 W of pump threshold [here](https://doi.org/10.1364/OL.32.002623).

This scheme would be useful only if we inject the auxiliary wavelength from
the beam splitter side and beat note is taken between the seed laser pick
offs at 1064 nm.

One concern that was raised is that the downconversion would actually
happen into various different frequencies separated by FSR of the cavity.
This is true. It would behave like a small frequency comb with extents upto
which the Quasi Phase Matching of the crystal is good enough. However, any
of the comb teeths can be used to lock to the arm cavity. The FSR of the
small crystal cavity would be in GHz and we should be fine with multi-color
auxiliary beam.

#### Open questions:
- [ ] Is it even possible to curve the crystal edges with required
radius of curvature (RoC)? For a 50mm crystal, if we can have RoC of
26mm on each side, we can achieve 9.5 kHz linewidth and 36 um beam waist.
[LinearCavitySim](http://sestei.github.io/jscav/?cav_R1=99.9998&cav_R2=99.9998&cav_L=0.05&cav_RoC1=0.026&cav_RoC2=0.026&cav_lambda0=810)
- [ ] How good the 810 nm coatings be made and coated on the crystal? Can
we use bragg mirror like coating to achieve high reflectivity?
- [ ] Can we create about 3 W of 532 nm light from 1064 nm without a cavity
based second harmonic generation process? Are our seed lasers powerful enough for that?

### PSROPO Schemes

<img src="schemes/ALSwithPSROPO.svg" alt="PSROPO_I" width="400"/> <img src="schemes/ALSwithPSROPO2.svg" alt="PSROPO_II" width="400"/>

We can create AUX by Pump-Signal Resonant OPO (PSROPO) in the 1.4 um region. [TOPICA sells <100 kHz tunable lasers](https://www.toptica.com/products/tunable-diode-lasers/frequency-converted-lasers/topo/) with such SROPO, so if we use our PSL's 1064 NPRO as seed laser with much less linewidth, to begin with, we might be able to employ a good PSROPO with good enough PZT and temperature controls.
Here, scheme II is much better which just uses 1 seed laser for everything and only 1 PSROPO. The AUX that goes to the cavity is upshifted by an AOM through a VCO which can be used to lock this AUX to 40m cavity. Here the challenge would be to bring down noise injected by VCO or suppress it effectively with a good loop. But if this and the above challenge are met, this is a promising scheme.

#### Challenges:
- [ ] How good can be make SROPO? How low linewidth can we achieve?
- [ ] Need to see what VCO can achieve low noise AOM upshifting and frequency actuation simultaneously.
- [ ] Maybe suppress VCO noise through a different kind of loop filter.

### Transfer Cavity scheme:
<img src="https://git.ligo.org/40m/als/raw/master/voyager/schemes/ALSwithTransferCavity.svg" alt="TC_I" width="400"/> <img src="https://git.ligo.org/40m/als/raw/master/voyager/schemes/ALSwithTransferCavityVCO.svg" alt="TC_I_VCO" width="400"/>

Use arbitrary AUX frequency in the allowed region and use a transfer cavity to compare the two frequencies. This transfer cavity would require a second AUX laser to be locked to it while the cavity itself is locked to PSL (Scheme I).

This can be made better by using VCO actuated AOM to upshift and create AUX beam for locking to 40m cavity, getting rid of most of the residual frequency noise of AUX laser.

[This group](https://doi.org/10.1364/JOSAB.35.000454) was able to achieve 10-kHz level linewidths on slave lasers through the transfer cavity by just using a PZT. So it seems doable.

Another idea (thanks to Jamie) is to use the Mode cleaner itself as the transfer cavity.
#### Problems with using Mode Cleaner as Transfer Cavity
* Locking to Mode Cleaner is difficult.
* Would require angular controls on the AUX2 beam to be locked to mode cleaner.
* Mode cleaner mirrors are large. Would require special coatings on these large mirrors even to test out the idea.



### Frequency comb scheme:
<img src="schemes/ALSwithFreqComb.svg" alt="FreqComb" width="400"/>

Use arbtrary AUX frequency in the allowed region and use a frequency comb to compare the two frequencies.

#### Findings:
User feedback about Frequency Combs from other groups (a.k.a. gossip)

| [Pros for one of these](https://www.menlosystems.com/products/)| [Cons for one of these](https://www.menlosystems.com/products/) |
| ------ | ------ |
| Lock can be robust over a few days.  | Brand new comb had faulty batch of fibers, which had to be replaced.  |
|  | Switching between amplifiers with ext. 5V supply, not pretty. |
|  | $$$ |

#### Cons:
* Too costly.

### Interesting/relevant papers

* [Optical parametric oscillator frequency tuning and control (1991)](https://doi.org/10.1364/JOSAB.8.000646)
