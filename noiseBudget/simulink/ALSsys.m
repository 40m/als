function retTF = ALSsys(c, L, L_MC, lambda, lambda_AUX, f, ...
                        outputPoint, inputPoint, varargin)
%
% Function to run simulink model and calculate transfer
% functions from noise injection points to residual
% displacement noise.
% Designed to be run from python.

% All extra arguments after inputPoint are assumed to be name-value pairs
% These would be made available in base workspace. This is for possible
% use by simulink model

for k=1:numel(varargin)/2
    if isstruct(varargin{2*k})
        if (isfield(varargin{2*k}, 'z') && ...
            isfield(varargin{2*k}, 'p') && ...
            isfield(varargin{2*k}, 'k'))
            varargin{2*k}.sys = zpk(-varargin{2*k}.z, ...
                                    -varargin{2*k}.p, ...
                                    varargin{2*k}.k);
        end
    end
    assignin('base', varargin{2*k-1}, varargin{2*k})
end

alsCtrl = yaml.ReadYaml('../ALS_controls.yaml');
subSys = fieldnames(alsCtrl);
for k=1:numel(subSys)
    % Skip parameters if they were provided as arguments
    if ~any(strcmp(varargin(1:2:end), subSys{k}))
        zpkFields = fieldnames(alsCtrl.(subSys{k}));
        if ~any(strcmp(zpkFields, 'z'))
            alsCtrl.(subSys{k}).z = [];
        else
            tmp=[];
            for n=1:numel(alsCtrl.(subSys{k}).z)
                value = alsCtrl.(subSys{k}).z(n);
                if iscell(value)
                    value = cell2mat(value);
                end
                if ischar(value)
                    value = eval(value);
                end
                tmp(n) = 2*pi*value;
            end
            alsCtrl.(subSys{k}).z = tmp;
        end
        if ~any(strcmp(zpkFields, 'p'))
            alsCtrl.(subSys{k}).p = [];
        else
            tmp=[];
            for n=1:numel(alsCtrl.(subSys{k}).p)
                value = alsCtrl.(subSys{k}).p(n);
                if iscell(value)
                    value = cell2mat(value);
                end
                if ischar(value)
                    value = eval(value);
                end
                tmp(n) = 2*pi*value;
            end
            alsCtrl.(subSys{k}).p = tmp;
        end
        if any(strcmp(zpkFields, 'delay'))
            [num, den] = pade(alsCtrl.(subSys{k}).delay, 4);
            alsCtrl.(subSys{k}).delay = tf(num,den);
        end
        alsCtrl.(subSys{k}).sys = zpk(-alsCtrl.(subSys{k}).z,...
                                      -alsCtrl.(subSys{k}).p,...
                                      alsCtrl.(subSys{k}).k);
        assignin('base', subSys{k}, alsCtrl.(subSys{k}))
    end
end

% conversion from meter to Hz at PSL wavelength
assignin('base', 'mtoHz', c /lambda / L);
% conversion from meter to Hz at AUX wavelength
assignin('base', 'mtoHz_AUX', c / (lambda_AUX) / L);
% conversion from meter to Hz at PSL wavelength for MC
assignin('base', 'mtoHz_MC', c / lambda / L_MC);
% conversion from meter to Hz at AUX wavelength for MC
assignin('base', 'mtoHz_MC_AUX', c / lambda_AUX / L_MC);
% conversion to get the actual displacement
assignin('base', 'Hztom', L * lambda / c);

% --------------------------------------------------------
% Makes the State-Space object from the .mdl

[ALSmodel.A,ALSmodel.B,ALSmodel.C,ALSmodel.D] = linmod2('ALS');
ALSmodel.sys = ss(ALSmodel.A,ALSmodel.B,ALSmodel.C,ALSmodel.D);

retTF = sys2TF(ALSmodel.sys(outputPoint, inputPoint), f);
end