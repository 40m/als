#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import sys, os
from os.path import join
cwd = os.getcwd()
sys.path.append('../../../pygwinc')
marinerIfo = os.path.abspath('../../../Voyager/mariner40/pygwinc-Mariner/FPMI')

import numpy as np
import scipy.constants as scc
import matplotlib as mpl
import matplotlib.pyplot as plt
# Load MPL style file
notLoaded = True
if notLoaded:
    mpl.style.use('../noiseBudget/style40.mpl')
    notLoaded = False

import gwinc
import yaml
import control
import control.matlab as mat
import matlab.engine


# ## Parameters

# In[ ]:


R = 4500.   #coil driver series resistance (Ohm)  
P = 20.     #input power (W)        
phi = -0.01 #SRC detuning (deg)
zeta = 0.   #homodyne phase (deg)


# ## Evaluate noise budget to get fundamental noise sources

# In[ ]:


# Define the frequency bins [Hz]
freq = np.logspace(-2, 3, 3000)

Budget = gwinc.load_budget(marinerIfo)
Budget.ifo.Optics.ITM.Transmittance = 1e-3
Budget.ifo.Optics.Quadrature.dc = 90 * np.pi/180 # 90 deg = usual phase quadrature readout

traces = Budget(freq).run()

freq2disp = Budget.ifo.Infrastructure.Length * Budget.ifo.Laser.Wavelength / scc.c
def Hztom(x):
    return x * freq2disp

def mtoHz(x):
    return x / freq2disp


# In[ ]:


dataDir = '../noiseBudget/data/'
def createBaseNoiseBudget(traces, alsTF):
    nosbud = {}
    # Suspension Thermal Noise
    nosbud['SuspensionThermal'] = ((traces['SuspensionThermal'][0]
                                    * np.abs(alsTF('COM_DISP'))**2),
                                   {'label': 'Suspension Thermal', 'linewidth': '4',
                                    'linestyle': '-.'})
    # Seismic Noise
    nosbud['Seismic'] = ((np.nan_to_num(traces['Seismic40'][0], nan=0)
                          * np.abs(alsTF('COM_DISP'))**2),
                         {'label': 'Seismic', 'linewidth': '4', 'linestyle': '-.'})
    # Coating Brownian Noise
    nosbud['CoatingBrownian'] = ((traces['CoatingBrownian'][0]
                                  * np.abs(alsTF('DIFF_DISP'))**2),
                                 {'label': 'Coatings Brownian', 'linewidth': '4',
                                  'linestyle': '-.'})
    # Coating Thermo-Optic Noise
    nosbud['CoatingThermoOptic'] = ((traces['CoatingThermoOptic'][0]
                                     * np.abs(alsTF('DIFF_DISP'))**2),
                                    {'label': 'Coatings ThermoOptic', 'linewidth': '4',
                                     'linestyle': '-.'})
    # Residual Frequency Noise of AUX
    try:
        nosbud['AUX Residual Freq Noise'] = ((1e4 / freq * np.abs(alsTF('AUX_Freq')))**2,
                                             {'label': 'AUX Res. Freq.',
                                              'linewidth': '4', 'linestyle': '-'})
    except BaseException:
        pass
    try:
        nosbud['PSL Residual Freq Noise'] = ((1e4 / freq * np.abs(alsTF('PSL_Freq')))**2,
                                             {'label': 'PSL Res. Freq.',
                                              'linewidth': '4', 'linestyle': '-'})
    except BaseException:
        pass
    
    # PSL DOPO Noise
    nosbud['DOPO PSL'] = ((freq*1e-5)**2 * np.abs(alsTF('PSL_DOPO'))**2,
                                 {'label': 'DOPO PSL', 'linewidth': '4', 'linestyle': '--'})
    
    # Beat PD Dark Noise
    V_RF = 22e-3                  # in Vrms
    S_V_Dark = (24e-9)**2         # in Vrms^2/Hz INPUT REFERED
    nosbud['Beat PD Dark'] = ((2 * S_V_Dark * freq**2 / V_RF**2) * np.abs(alsTF('BEAT_PD'))**2,
                              {'label': 'Beat PD Dark', 'linewidth': '4', 'linestyle': '-'})
    # Beat PD Shot Noise
    S_V_Shot = (8e-9)**2          # in Vrms^2/Hz INPUT REFERED
    nosbud['Beat PD Shot'] = ((2 * S_V_Shot * freq**2 / V_RF**2) * np.abs(alsTF('BEAT_PD'))**2,
                              {'label': 'Beat PD Shot', 'linewidth': '4', 'linestyle': '-'})
    # DFD Noise
    DFDnoise = np.loadtxt(dataDir + 'DFDnoise.txt')   
    DFDnoise = np.interp(freq, DFDnoise[:, 0], DFDnoise[:, 1])
    nosbud['DFD'] = ((DFDnoise * np.abs(alsTF('BEAT_PD')))**2,
                     {'label': 'DFD', 'linewidth': '4', 'linestyle': '-'})
    # ADC Noise
    ADCnoise = np.loadtxt(dataDir + 'ADCnoiseInputRef.txt')   # in V/rtHz
    ADCnoise = np.interp(freq, ADCnoise[:, 0], ADCnoise[:, 1])
    nosbud['ADC'] = ((ADCnoise * np.abs(alsTF('ALS_Filt')))**2,
                     {'label': 'ADC', 'linewidth': '4', 'linestyle': '-'})
    # DAC Noise
    DACnoise = np.loadtxt(dataDir + 'DACwithWhitening.txt')
    DACnoise = np.interp(freq, DACnoise[:, 0], DACnoise[:, 1])
    nosbud['DAC'] = ((DACnoise * np.abs(alsTF('ALS_DAC')))**2,
                     {'label': 'DAC', 'linewidth': '4', 'linestyle': '-'})
    return nosbud

def calcTotalNoise(nosbud, style, freq=freq):
    totNoise = np.zeros_like(freq)
    for noise in nosbud:
        if noise is not 'Total':
            totNoise += nosbud[noise][0]
    nosbud['Total'] = (totNoise, style)
    intTotal = np.zeros_like(totNoise)
    for ii in range(len(freq)-1, 0, -1):
        intTotal[ii-1] = totNoise[ii] * (freq[ii] - freq[ii-1]) + intTotal[ii]
    nosbud['intTotal'] = (intTotal,
                          {'label': 'RMS', 'color': '#000000',
                           'linewidth': '4', 'linestyle': '--'})
    
def plotNB(nosbud, freq=freq):
    fig, ax = plt.subplots(1,1, figsize=(14,9))

    # Title
    ax.set_title(nosbud['Total'][1]['label']
                 + '\nOut-of-loop (OOL) Residual Arm Length Motion\n'
                 + 'with respect to Main Laser Frequency')


    # Noise curves
    for key in nosbud:
        trace = nosbud[key][0]**.5
        style = nosbud[key][1]
        if key == 'Total':
            ax.plot(freq, trace, label='Total', lw=4, color='k')
        else:
            ax.plot(freq, trace, **style)

    # Plot formatting
    ax.set_xlim([freq[0], freq[-1]])
    ax.set_ylim([1e-18, 3e-11])
    ax.set_xlabel(u"Frequency [Hz]")
    ax.set_ylabel(u"OOL Residual Arm Length Noise ASD [m/\u221AHz]")
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.legend(ncol=1, loc=(-0.5, 0.0));
    ax.tick_params(right=False)
    secaxy = ax.secondary_yaxis('right', functions=(mtoHz, Hztom))
    secaxy.set_ylabel(u"Residual Frequency Difference Noise ASD [Hz/\u221AHz]")

    RMSNoise = nosbud['intTotal'][0][0]**.5
    RMSNoiseFreq = mtoHz(RMSNoise)
    RMSstr = str(np.round(RMSNoise*1e12, 1))+' pm ('+str(np.round(RMSNoiseFreq, 1))+' Hz)'

    ax.text(0.2, RMSNoise*2, RMSstr, fontsize='x-large')
    return fig


# ---
# ## Fractional Harmonic Generation Scheme I
# 
# <img src="./simulink/vALS_FHG_I.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = lambda_PSL / 2
try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_FHG_I = eng.runSimModel('vALS_FHG_I', '../../noiseBudget/ALS_controls.yaml',
                                 'mtoHz', scc.c /lambda_PSL / L,
                                 'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                 'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                 'Hztom', L * lambda_PSL / scc.c)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_FHG_I[key] = np.asarray(vALS_FHG_I[key])

vALS_FHG_I['ss'] = control.ss(vALS_FHG_I['A'], vALS_FHG_I['B'], vALS_FHG_I['C'], vALS_FHG_I['D'])
vALS_FHG_I['TFmag'], vALS_FHG_I['TFph'], _ = control.freqresp(vALS_FHG_I['ss'], 2 * np.pi * freq)
vALS_FHG_I['TFcom'] = vALS_FHG_I['TFmag'] * np.exp(1j * vALS_FHG_I['TFph'])
    
def vALS_FHG_I_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_FHG_I['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_FHG_I['outPorts'][outPort]-1, vALS_FHG_I['inPorts'][inPort]-1, : ]
    else:
        return vALS_FHG_I['TFcom'][vALS_FHG_I['outPorts'][outPort]-1, vALS_FHG_I['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_FHG_I_nb = createBaseNoiseBudget(traces, vALS_FHG_I_TF)


# #### Add extra noise sources
# 
# Upper limit of SHG noise is assumed to be $1\times10^{-5}f\,\text{Hz}/\sqrt{\text{Hz}}$ as per [Optics Express Vol. 20, Issue 19, pp. 21019-21024 (2012)](https://doi.org/10.1364/OE.20.021019). Assuming most of this noise comes from thermal motion causing mode mismatching (due to changes in incidence angle) and assuming the tolerance to incidence angle is same for SHG and SFG, we assume noise introduced by SFG and DOPO are same as SHG.
# 
# On the AUX path, these non-linear processes happen at four places.

# In[ ]:


vALS_FHG_I_nb['DOPO AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_I_TF('AUX_DOPO'))**2,
                             {'label': 'DOPO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_FHG_I_nb['SFG AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_I_TF('AUX_SFG'))**2,
                            {'label': 'SFG AUX', 'linewidth': '4', 'linestyle': ':'})
vALS_FHG_I_nb['DOPO2 AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_I_TF('AUX_DOPO2'))**2,
                              {'label': 'DOPO2 AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_FHG_I_nb['SHG AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_I_TF('AUX_SHG'))**2,
                            {'label': 'SHG AUX', 'linewidth': '4', 'linestyle': ':'})


# Other than the 2128 nm generating DOPO which has been added, PSL path requires an additional SFG stage for beat note.

# In[ ]:


vALS_FHG_I_nb['SFG PSL'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_I_TF('PSL_SFG'))**2,
                            {'label': 'SFG PSL', 'linewidth': '4', 'linestyle': ':'})


# In[ ]:


style = {'label': 'FHG I', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_FHG_I_nb, style)


# In[ ]:


plotNB(vALS_FHG_I_nb);


# ---
# ## Fractional Harmonic Generation Scheme Ib
# 
# <img src="./simulink/vALS_FHG_Ib.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = lambda_PSL / 2

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 1.51
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_FHG_Ib = eng.runSimModel('vALS_FHG_Ib', '../../noiseBudget/ALS_controls.yaml',
                                  'mtoHz', scc.c /lambda_PSL / L,
                                  'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                  'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                  'Hztom', L * lambda_PSL / scc.c,
                                  'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_FHG_Ib[key] = np.asarray(vALS_FHG_Ib[key])

vALS_FHG_Ib['ss'] = control.ss(vALS_FHG_Ib['A'], vALS_FHG_Ib['B'], vALS_FHG_Ib['C'], vALS_FHG_Ib['D'])
vALS_FHG_Ib['TFmag'], vALS_FHG_Ib['TFph'], _ = control.freqresp(vALS_FHG_Ib['ss'], 2 * np.pi * freq)
vALS_FHG_Ib['TFcom'] = vALS_FHG_Ib['TFmag'] * np.exp(1j * vALS_FHG_Ib['TFph'])
    
def vALS_FHG_Ib_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_FHG_Ib['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_FHG_Ib['outPorts'][outPort]-1, vALS_FHG_Ib['inPorts'][inPort]-1, : ]
    else:
        return vALS_FHG_Ib['TFcom'][vALS_FHG_Ib['outPorts'][outPort]-1, vALS_FHG_Ib['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_FHG_Ib_nb = createBaseNoiseBudget(traces, vALS_FHG_Ib_TF)


# #### Add extra noise sources
# 
# Upper limit of SHG noise is assumed to be $1\times10^{-5}f\,\text{Hz}/\sqrt{\text{Hz}}$ as per [Optics Express Vol. 20, Issue 19, pp. 21019-21024 (2012)](https://doi.org/10.1364/OE.20.021019). Assuming most of this noise comes from thermal motion causing mode mismatching (due to changes in incidence angle) and assuming the tolerance to incidence angle is same for SHG and SFG, we assume noise introduced by SFG and DOPO are same as SHG.
# 
# On the AUX path, these non-linear processes happen at three places.

# In[ ]:


vALS_FHG_Ib_nb['DOPO AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_Ib_TF('AUX_DOPO'))**2,
                              {'label': 'DOPO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_FHG_Ib_nb['SFG AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_Ib_TF('AUX_SFG'))**2,
                             {'label': 'SFG AUX', 'linewidth': '4', 'linestyle': ':'})
vALS_FHG_Ib_nb['DOPO2 AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_Ib_TF('AUX_DOPO2'))**2,
                               {'label': 'DOPO2 AUX', 'linewidth': '4', 'linestyle': '--'})


# In[ ]:


style = {'label': 'FHG Ib', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_FHG_Ib_nb, style)


# In[ ]:


plotNB(vALS_FHG_Ib_nb);


# ---
# ## Fractional Harmonic Generation Scheme II
# 
# <img src="./simulink/vALS_FHG_II.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = lambda_PSL / 2
# Increase gain by 2 as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_FHG_II = eng.runSimModel('vALS_FHG_II', '../../noiseBudget/ALS_controls.yaml',
                                  'mtoHz', scc.c /lambda_PSL / L,
                                  'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                  'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                  'Hztom', L * lambda_PSL / scc.c,
                                  'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_FHG_II[key] = np.asarray(vALS_FHG_II[key])

vALS_FHG_II['ss'] = control.ss(vALS_FHG_II['A'], vALS_FHG_II['B'], vALS_FHG_II['C'], vALS_FHG_II['D'])
vALS_FHG_II['TFmag'], vALS_FHG_II['TFph'], _ = control.freqresp(vALS_FHG_II['ss'], 2 * np.pi * freq)
vALS_FHG_II['TFcom'] = vALS_FHG_II['TFmag'] * np.exp(1j * vALS_FHG_II['TFph'])
    
def vALS_FHG_II_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_FHG_II['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_FHG_II['outPorts'][outPort]-1, vALS_FHG_II['inPorts'][inPort]-1, : ]
    else:
        return vALS_FHG_II['TFcom'][vALS_FHG_II['outPorts'][outPort]-1, vALS_FHG_II['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_FHG_II_nb = createBaseNoiseBudget(traces, vALS_FHG_II_TF)


# #### Add extra noise sources
# 
# Upper limit of SHG noise is assumed to be $1\times10^{-5}f\,\text{Hz}/\sqrt{\text{Hz}}$ as per [Optics Express Vol. 20, Issue 19, pp. 21019-21024 (2012)](https://doi.org/10.1364/OE.20.021019). Assuming most of this noise comes from thermal motion causing mode mismatching (due to changes in incidence angle) and assuming the tolerance to incidence angle is same for SHG and SFG, we assume noise introduced by SFG and DOPO are same as SHG.
# 
# On the AUX path, these non-linear processes happen at three places.

# In[ ]:


vALS_FHG_II_nb['DOPO AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_II_TF('AUX_DOPO'))**2,
                              {'label': 'DOPO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_FHG_II_nb['SFG AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_II_TF('AUX_SFG'))**2,
                             {'label': 'SFG AUX', 'linewidth': '4', 'linestyle': ':'})
vALS_FHG_II_nb['DOPO2 AUX'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_II_TF('AUX_DOPO2'))**2,
                               {'label': 'DOPO2 AUX', 'linewidth': '4', 'linestyle': '--'})


# Other than the 2128 nm generating DOPO which has been added, PSL path requires an additional SFG stage and a DOPO stage for beat note.

# In[ ]:


vALS_FHG_I_nb['SFG PSL'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_I_TF('PSL_SFG'))**2,
                            {'label': 'SFG PSL', 'linewidth': '4', 'linestyle': ':'})
vALS_FHG_II_nb['DOPO2 PSL'] = ((freq*1e-5)**2 * np.abs(vALS_FHG_II_TF('PSL_DOPO2'))**2,
                               {'label': 'DOPO2 PSL', 'linewidth': '4', 'linestyle': '--'})


# In[ ]:


style = {'label': 'FHG II', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_FHG_II_nb, style)


# In[ ]:


plotNB(vALS_FHG_II_nb);


# ---
# ## SROPO Scheme Ib
# 
# <img src="./simulink/vALS_SROPO_Ib.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9
SROPO = 532e-9 / lambda_AUX

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_SROPO_Ib = eng.runSimModel('vALS_SROPO_Ib', '../../noiseBudget/ALS_controls.yaml',
                                    'mtoHz', scc.c /lambda_PSL / L,
                                    'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                    'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                    'Hztom', L * lambda_PSL / scc.c,
                                    'SROPO', SROPO,
                                    'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_SROPO_Ib[key] = np.asarray(vALS_SROPO_Ib[key])

vALS_SROPO_Ib['ss'] = control.ss(vALS_SROPO_Ib['A'], vALS_SROPO_Ib['B'], vALS_SROPO_Ib['C'], vALS_SROPO_Ib['D'])
vALS_SROPO_Ib['TFmag'], vALS_SROPO_Ib['TFph'], _ = control.freqresp(vALS_SROPO_Ib['ss'], 2 * np.pi * freq)
vALS_SROPO_Ib['TFcom'] = vALS_SROPO_Ib['TFmag'] * np.exp(1j * vALS_SROPO_Ib['TFph'])
    
def vALS_SROPO_Ib_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_SROPO_Ib['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_SROPO_Ib['outPorts'][outPort]-1, vALS_SROPO_Ib['inPorts'][inPort]-1, : ]
    else:
        return vALS_SROPO_Ib['TFcom'][vALS_SROPO_Ib['outPorts'][outPort]-1, vALS_SROPO_Ib['inPorts'][inPort]-1, : ]


# #### Create base noise budget with common noises

# In[ ]:


vALS_SROPO_Ib_nb = createBaseNoiseBudget(traces, vALS_SROPO_Ib_TF)


# #### Add extra noise sources
# 
# In this scheme, AUX and reference for AUX are both generated through identical SROPO adding noises at AUX_OPO. Assuming noise of these are same as SFG for now. There is additional 532 generation in SHG process.

# In[ ]:


vALS_SROPO_Ib_nb['OPO AUX'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_Ib_TF('AUX_OPO'))**2,
                               {'label': 'OPO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_SROPO_Ib_nb['SHG AUX'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_Ib_TF('AUX_SHG'))**2,
                               {'label': 'SHG AUX', 'linewidth': '4', 'linestyle': ':'})


# In[ ]:


style = {'label': 'SROPO Ib', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_SROPO_Ib_nb, style)


# In[ ]:


fig = plotNB(vALS_SROPO_Ib_nb);
fig.gca().set_title('Single Resonant OPO Scheme I Noise Budget')


# ---
# ## SROPO Scheme I
# 
# <img src="./simulink/vALS_SROPO_I.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9
SROPO = 532e-9 / lambda_AUX

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2.93
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_SROPO_I = eng.runSimModel('vALS_SROPO_I', '../../noiseBudget/ALS_controls.yaml',
                                   'mtoHz', scc.c /lambda_PSL / L,
                                   'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                   'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                   'Hztom', L * lambda_PSL / scc.c,
                                   'SROPO', SROPO,
                                   'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_SROPO_I[key] = np.asarray(vALS_SROPO_I[key])

vALS_SROPO_I['ss'] = control.ss(vALS_SROPO_I['A'], vALS_SROPO_I['B'], vALS_SROPO_I['C'], vALS_SROPO_I['D'])
vALS_SROPO_I['TFmag'], vALS_SROPO_I['TFph'], _ = control.freqresp(vALS_SROPO_I['ss'], 2 * np.pi * freq)
vALS_SROPO_I['TFcom'] = vALS_SROPO_I['TFmag'] * np.exp(1j * vALS_SROPO_I['TFph'])
    
def vALS_SROPO_I_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_SROPO_I['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_SROPO_I['outPorts'][outPort]-1, vALS_SROPO_I['inPorts'][inPort]-1, : ]
    else:
        return vALS_SROPO_I['TFcom'][vALS_SROPO_I['outPorts'][outPort]-1, vALS_SROPO_I['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_SROPO_I_nb = createBaseNoiseBudget(traces, vALS_SROPO_I_TF)


# #### Add extra noise sources
# 
# In this scheme, AUX and reference for AUX are both generated through identical SROPO adding noises at AUX_OPO and PSL_OPO inputs. Assuming noise of these are same as SFG for now. There are additional 532 generation in SHG process.

# In[ ]:


vALS_SROPO_I_nb['OPO AUX'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_I_TF('AUX_OPO'))**2,
                              {'label': 'OPO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_SROPO_I_nb['OPO PSL'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_I_TF('PSL_OPO'))**2,
                              {'label': 'OPO PSL', 'linewidth': '4', 'linestyle': ':'})
vALS_SROPO_I_nb['SHG AUX'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_I_TF('AUX_SHG'))**2,
                              {'label': 'SHG AUX', 'linewidth': '4', 'linestyle': ':'})
vALS_SROPO_I_nb['SHG PSL'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_I_TF('PSL_SHG'))**2,
                              {'label': 'SHG PSL', 'linewidth': '4', 'linestyle': ':'})


# In[ ]:


style = {'label': 'SROPO I', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_SROPO_I_nb, style)


# In[ ]:


fig = plotNB(vALS_SROPO_I_nb);
fig.gca().set_title('Single Resonant OPO Scheme I Noise Budget')


# ---
# ## SROPO Scheme II
# 
# <img src="./simulink/vALS_SROPO_II.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9
SROPO = 532e-9 / lambda_AUX

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 5.97
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

# VCO transfer function
A_VCO = {'z': matlab.double([]),
         'p': matlab.double([]),
         'k': 100e3      # Maximum 100kHz/V actuation by Marconi 2023A
        }

# 40m AUX locking filter
F_AUX ={'z': matlab.double([1.0e4, 100, 1]),
        'p': matlab.double([2, 1.2, 1e-4]),
        'k': 0.0504 * 5e6 / A_VCO['k']  # Compensating lost gain due to less actuation
       }
try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_SROPO_II = eng.runSimModel('vALS_SROPO_II', '../../noiseBudget/ALS_controls.yaml',
                                    'mtoHz', scc.c /lambda_PSL / L,
                                    'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                    'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                    'Hztom', L * lambda_PSL / scc.c,
                                    'SROPO', SROPO,
                                    'A_VCO', A_VCO,
                                    'F_AUX', F_AUX,
                                    'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_SROPO_II[key] = np.asarray(vALS_SROPO_II[key])

vALS_SROPO_II['ss'] = control.ss(vALS_SROPO_II['A'], vALS_SROPO_II['B'], vALS_SROPO_II['C'], vALS_SROPO_II['D'])
vALS_SROPO_II['TFmag'], vALS_SROPO_II['TFph'], _ = control.freqresp(vALS_SROPO_II['ss'], 2 * np.pi * freq)
vALS_SROPO_II['TFcom'] = vALS_SROPO_II['TFmag'] * np.exp(1j * vALS_SROPO_II['TFph'])
    
def vALS_SROPO_II_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_SROPO_II['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_SROPO_II['outPorts'][outPort]-1, vALS_SROPO_II['inPorts'][inPort]-1, : ]
    else:
        return vALS_SROPO_II['TFcom'][vALS_SROPO_II['outPorts'][outPort]-1, vALS_SROPO_II['inPorts'][inPort]-1, : ]


# In[ ]:


#### Create bease noise budget with common noises


# In[ ]:


vALS_SROPO_II_nb = createBaseNoiseBudget(traces, vALS_SROPO_II_TF)


# #### Add extra noise sources
# 
# In this scheme, reference for AUX is generated through an SROPO adding noises at SROPO inputs while AUX is generated by picking off this reference, upshifting it with an AOM and controlling the frequency through a VCO. The VCO would add frequency noise at AUX_VCO.
# 
# For now, assuming noise due to SROPO is same as that due to SHG.
# 
# A measurement of frequency noise of Marconi 2023A was made in this [elog](https://nodus.ligo.caltech.edu:8081/CTN/2286) at different actuation slopes. Taking worst case scenario for high actuaiton slope of 100 kHz/V, $100 mHz/\sqrt{Hz}$ flat noise can be assumed. This is what needs to be minimized. If the 40m can be locked with lower actuation slope, it would be nice.

# In[ ]:


vALS_SROPO_II_nb['VCO AUX'] = ((0.1 * np.abs(vALS_SROPO_II_TF('AUX_VCO')))**2,
                               {'label': 'VCO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_SROPO_II_nb['SROPO'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_II_TF('SROPO'))**2,
                             {'label': 'SROPO', 'linewidth': '4', 'linestyle': ':'})
vALS_SROPO_I_nb['SHG'] = ((freq*1e-5)**2 * np.abs(vALS_SROPO_II_TF('SHG'))**2,
                          {'label': 'SHG', 'linewidth': '4', 'linestyle': ':'})


# In[ ]:


style = {'label': 'SROPO II', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_SROPO_II_nb, style)


# In[ ]:


fig = plotNB(vALS_SROPO_II_nb);
fig.gca().set_title('Single Resonant OPO Scheme II Noise Budget')


# ---
# ## Frequency Comb Scheme IIb
# 
# <img src="./simulink/vALS_FreqComb_IIb.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9

f_CEO = 10e6        # Frequency Comb CEO Frequency
f_Rep = 250e6       # Frequency Comb Repitition Rate
f_Seed = scc.c/lambda_Seed   # PSL Frequency
f_AUX = scc.c/lambda_AUX                    # AUX Frequency
m1 = (f_Seed - f_CEO)//f_Rep  # Nearest FC mode number to PSL
m2 = (f_AUX - f_CEO)//f_Rep  # Nearest FC mode number to AUX
DDS = {'k': round(m2 / m1 * 2**48) / 2**48} # DDS Frequency Tuning Word
print('Frequency Tuning Word to be written on DDS Chip')
print('{:048b}'.format(int(round(m2 / m1 * 2**48))))

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2.92
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_FreqComb_IIb = eng.runSimModel('vALS_FreqComb_IIb', '../../noiseBudget/ALS_controls.yaml',
                                        'mtoHz', scc.c /lambda_PSL / L,
                                        'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                        'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                        'Hztom', L * lambda_PSL / scc.c,
                                        'm1', m1, 'm2', m2, 'DDS', DDS,
                                        'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_FreqComb_IIb[key] = np.asarray(vALS_FreqComb_IIb[key])

vALS_FreqComb_IIb['ss'] = control.ss(vALS_FreqComb_IIb['A'], vALS_FreqComb_IIb['B'], vALS_FreqComb_IIb['C'], vALS_FreqComb_IIb['D'])
vALS_FreqComb_IIb['TFmag'], vALS_FreqComb_IIb['TFph'], _ = control.freqresp(vALS_FreqComb_IIb['ss'], 2 * np.pi * freq)
vALS_FreqComb_IIb['TFcom'] = vALS_FreqComb_IIb['TFmag'] * np.exp(1j * vALS_FreqComb_IIb['TFph'])
    
def vALS_FreqComb_IIb_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_FreqComb_IIb['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_FreqComb_IIb['outPorts'][outPort]-1, vALS_FreqComb_IIb['inPorts'][inPort]-1, : ]
    else:
        return vALS_FreqComb_IIb['TFcom'][vALS_FreqComb_IIb['outPorts'][outPort]-1, vALS_FreqComb_IIb['inPorts'][inPort]-1, : ]


# #### Create base noise budget with common noises

# In[ ]:


vALS_FreqComb_IIb_nb = createBaseNoiseBudget(traces, vALS_FreqComb_IIb_TF)


# #### Add extra noise sources
# In this scheme, there is frequency comb adding noise at <b><span style="color:red">f_CEO</span></b> for Carrier Envelope Offset frequency noise and <b><span style="color:red">f_Rep</span></b> for repitition rate frequency noise. Above, we have used parameters for [Menlo FC1500-250-ULN](https://www.menlosystems.com/products/optical-frequency-combs/fc1500-250-uln/).
# 
# However, in the scheme as it is mentiond right now, the noise due to carrier envelope offset frequency would get cancelled as can be verified by calculating the transfer function from <b><span style="color:red">f_CEO</span></b>.

# In[ ]:


np.abs(vALS_FreqComb_IIb_TF('f_CEO'))


# The only extra noise contribution comes from noise in repition rate of the frequency comb which directly comes from the RF reference used by the frequency comb. Assuming this RF reference would be [SRS FS725 Rb Clock](https://www.thinksrs.com/products/fs725.html), the phase noise of RF reference is
# * <–130dBc/Hz (10Hz)
# * <–140dBc/Hz (100Hz)
# * <–150dBc/Hz (1kHz)
# * <–155dBc/Hz (10kHz)
# 
# However, the effect of repitition rate noise would get minimized by 15 (Yes!) orders of magnitude due to the cancellation used here.

# In[ ]:


vALS_FreqComb_IIb_nb['f_Rep'] = ((7e-5 / freq)**2 * np.abs(vALS_FreqComb_IIb_TF('f_Rep'))**2,
                                 {'label': 'f_Rep FC', 'linewidth': '4', 'linestyle': '-'})


# This scheme has more sources of noise added. SHG on frequency comb would add noise at <b><span style="color:red">FC_SHG</span></b>.

# In[ ]:


vALS_FreqComb_IIb_nb['FC_SHG'] = ((freq*1e-5)**2 * np.abs(vALS_FreqComb_IIb_TF('FC_SHG'))**2,
                                 {'label': 'SHG f_CEO', 'linewidth': '4', 'linestyle': '-'})


# Further, there is additional timing noise due to DDS added at <b><span style="color:red">DDS</span></b>. For DDS [AD9956](https://www.analog.com/en/products/ad9956.html), the additional noise added by this chip looks comparable to Rb clock which would be required to clock the cip as well. So overall, there should be $\sqrt{2}$ as much noise due to Rb clock alone.

# In[ ]:


vALS_FreqComb_IIb_nb['DDS'] = (2 * (7e-5 / freq)**2 * np.abs(vALS_FreqComb_IIb_TF('DDS'))**2,
                                 {'label': 'DDS', 'linewidth': '4', 'linestyle': '-'})


# In[ ]:


style = {'label': 'FreqComb IIb', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_FreqComb_IIb_nb, style)


# In[ ]:


fig = plotNB(vALS_FreqComb_IIb_nb);
fig.gca().set_title('Frequency Comb Scheme IIb Noise Budget')
#fig.savefig('vALS_FreqComb_IIb_nb.pdf', bbox_inches='tight')


# ---
# ## PSROPO Scheme I
# 
# <img src="./simulink/vALS_PSROPO_I.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9
PSROPO = lambda_Seed / lambda_AUX

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2.93
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_PSROPO_I = eng.runSimModel('vALS_PSROPO_I', '../../noiseBudget/ALS_controls.yaml',
                                    'mtoHz', scc.c /lambda_PSL / L,
                                    'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                    'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                    'Hztom', L * lambda_PSL / scc.c,
                                    'PSROPO', PSROPO,
                                    'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_PSROPO_I[key] = np.asarray(vALS_PSROPO_I[key])

vALS_PSROPO_I['ss'] = control.ss(vALS_PSROPO_I['A'], vALS_PSROPO_I['B'], vALS_PSROPO_I['C'], vALS_PSROPO_I['D'])
vALS_PSROPO_I['TFmag'], vALS_PSROPO_I['TFph'], _ = control.freqresp(vALS_PSROPO_I['ss'], 2 * np.pi * freq)
vALS_PSROPO_I['TFcom'] = vALS_PSROPO_I['TFmag'] * np.exp(1j * vALS_PSROPO_I['TFph'])
    
def vALS_PSROPO_I_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_PSROPO_I['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_PSROPO_I['outPorts'][outPort]-1, vALS_PSROPO_I['inPorts'][inPort]-1, : ]
    else:
        return vALS_PSROPO_I['TFcom'][vALS_PSROPO_I['outPorts'][outPort]-1, vALS_PSROPO_I['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_PSROPO_I_nb = createBaseNoiseBudget(traces, vALS_PSROPO_I_TF)


# #### Add extra noise sources
# 
# In this scheme, AUX and reference for AUX are both generated through identical PSROPO adding noises at AUX_OPO and PSL_OPO inputs. Assuming noise of these are same as SFG for now.

# In[ ]:


vALS_PSROPO_I_nb['OPO AUX'] = ((freq*1e-5)**2 * np.abs(vALS_PSROPO_I_TF('AUX_OPO'))**2,
                               {'label': 'OPO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_PSROPO_I_nb['OPO PSL'] = ((freq*1e-5)**2 * np.abs(vALS_PSROPO_I_TF('PSL_OPO'))**2,
                               {'label': 'OPO PSL', 'linewidth': '4', 'linestyle': ':'})


# In[ ]:


style = {'label': 'PSROPO I', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_PSROPO_I_nb, style)


# In[ ]:


plotNB(vALS_PSROPO_I_nb);


# ---
# ## PSROPO Scheme II
# 
# <img src="./simulink/vALS_PSROPO_II.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9
PSROPO = lambda_Seed / lambda_AUX

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2.93
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

# VCO transfer function
A_VCO = {'z': matlab.double([]),
         'p': matlab.double([]),
         'k': 100e3      # Maximum 100kHz/V actuation by Marconi 2023A
        }

# 40m AUX locking filter
F_AUX ={'z': matlab.double([1.0e4, 100, 1]),
        'p': matlab.double([2, 1.2, 1e-4]),
        'k': 0.0504 * 5e6 / A_VCO['k']  # Compensating lost gain due to less actuation
       }
try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_PSROPO_II = eng.runSimModel('vALS_PSROPO_II', '../../noiseBudget/ALS_controls.yaml',
                                     'mtoHz', scc.c /lambda_PSL / L,
                                     'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                     'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                     'Hztom', L * lambda_PSL / scc.c,
                                     'PSROPO', PSROPO,
                                     'A_VCO', A_VCO,
                                     'F_AUX', F_AUX,
                                     'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_PSROPO_II[key] = np.asarray(vALS_PSROPO_II[key])

vALS_PSROPO_II['ss'] = control.ss(vALS_PSROPO_II['A'], vALS_PSROPO_II['B'], vALS_PSROPO_II['C'], vALS_PSROPO_II['D'])
vALS_PSROPO_II['TFmag'], vALS_PSROPO_II['TFph'], _ = control.freqresp(vALS_PSROPO_II['ss'], 2 * np.pi * freq)
vALS_PSROPO_II['TFcom'] = vALS_PSROPO_II['TFmag'] * np.exp(1j * vALS_PSROPO_II['TFph'])
    
def vALS_PSROPO_II_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_PSROPO_II['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_PSROPO_II['outPorts'][outPort]-1, vALS_PSROPO_II['inPorts'][inPort]-1, : ]
    else:
        return vALS_PSROPO_II['TFcom'][vALS_PSROPO_II['outPorts'][outPort]-1, vALS_PSROPO_II['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_PSROPO_II_nb = createBaseNoiseBudget(traces, vALS_PSROPO_II_TF)


# #### Add extra noise sources
# 
# In this scheme, reference for AUX is generated through an PSROPO adding noises at PSROPO inputs while AUX is generated by picking off this reference, upshifting it with an AOM and controlling the frequency through a VCO. The VCO would add frequency noise at AUX_VCO.
# 
# For now, assuming noise due to PSROPO is same as that due to SHG.
# 
# A measurement of frequency noise of Marconi 2023A was made in this [elog](https://nodus.ligo.caltech.edu:8081/CTN/2286) at different actuation slopes. Taking worst case scenario for high actuaiton slope of 100 kHz/V, $100 mHz/\sqrt{Hz}$ flat noise can be assumed. This is what needs to be minimized. If the 40m can be locked with lower actuation slope, it would be nice.

# In[ ]:


vALS_PSROPO_II_nb['VCO AUX'] = ((0.1 * np.abs(vALS_PSROPO_II_TF('AUX_VCO')))**2,
                                {'label': 'VCO AUX', 'linewidth': '4', 'linestyle': '--'})
vALS_PSROPO_II_nb['PSROPO'] = ((freq*1e-5)**2 * np.abs(vALS_PSROPO_II_TF('PSROPO'))**2,
                                {'label': 'PSROPO', 'linewidth': '4', 'linestyle': ':'})


# In[ ]:


style = {'label': 'PSROPO II', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_PSROPO_II_nb, style)


# In[ ]:


plotNB(vALS_PSROPO_II_nb);


# ---
# ## Transfer Cavity Scheme I
# 
# <img src="./simulink/vALS_TransCav_I.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9         # AUX wavelength
L_TC = 100e-3                # Length of transfer cavity
FSR_TC = scc.c / (2 * L_TC)
Finesse_TC = 5e4             # Assuming same for both wavelengths
Pole_TC = FSR_TC / ( 2 * Finesse_TC)
Pole_TC_AUX = Pole_TC
mtoHz_TC = scc.c / Budget.ifo.Laser.Wavelength / L_TC
mtoHz_TC_AUX = scc.c / lambda_AUX / L_TC
PZT_FSR_Scan_Voltage = 75
PZT_Act_Slope = FSR_TC / PZT_FSR_Scan_Voltage # Hz/V
PZT_Disp_Slope = Budget.ifo.Laser.Wavelength / PZT_FSR_Scan_Voltage # m/V

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2.93
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

# Transfer Cavity Poles
TC_PSL = {'z': matlab.double([]),
          'p': matlab.double([Pole_TC]),
          'k': Pole_TC
         }
TC_AUX = {'z': matlab.double([]),
          'p': matlab.double([Pole_TC_AUX]),
          'k': Pole_TC_AUX
         }
# Transfer Cavity Locking Feedback Filter
F_TC = {'z': matlab.double([20e3, 20e3, 4e3]),
        'p': matlab.double([1e3, 1e3, 40]),
        'k': 0.5769
       }
F_AUX2 = {'z': matlab.double([1e4, 100, 1]),
          'p': matlab.double([2, 1.2, 1e-4]),
          'k': 0.0504
         }
# Cavity PZT transfer function [m/V]
A_TC_PZT = {'z': matlab.double([]),
            'p': matlab.double([]),
            'k': PZT_Disp_Slope
           }
# Transfer Cavity PDH discriminants
D_AUX_TC = {'z': matlab.double([]),
            'p': matlab.double([]),
            'k': 5e-6
           }
D_PSL_TC = {'z': matlab.double([]),
            'p': matlab.double([]),
            'k': 2.5e-5
           }
try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_TransCav_I = eng.runSimModel('vALS_TransCav_I', '../../noiseBudget/ALS_controls.yaml',
                                      'mtoHz', scc.c /lambda_PSL / L,
                                      'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                      'mtoHz_TC', mtoHz_TC,
                                      'mtoHz_TC_AUX', mtoHz_TC_AUX,
                                      'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                      'Hztom', L * lambda_PSL / scc.c,
                                      'TC_PSL', TC_PSL,
                                      'TC_AUX', TC_AUX,
                                      'F_TC', F_TC,
                                      'F_AUX2', F_AUX2,
                                      'A_TC_PZT', A_TC_PZT,
                                      'D_AUX_TC', D_AUX_TC,
                                      'D_PSL_TC', D_PSL_TC,
                                      'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_TransCav_I[key] = np.asarray(vALS_TransCav_I[key])

vALS_TransCav_I['ss'] = control.ss(vALS_TransCav_I['A'], vALS_TransCav_I['B'], vALS_TransCav_I['C'], vALS_TransCav_I['D'])
vALS_TransCav_I['TFmag'], vALS_TransCav_I['TFph'], _ = control.freqresp(vALS_TransCav_I['ss'], 2 * np.pi * freq)
vALS_TransCav_I['TFcom'] = vALS_TransCav_I['TFmag'] * np.exp(1j * vALS_TransCav_I['TFph'])
    
def vALS_TransCav_I_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_TransCav_I['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_TransCav_I['outPorts'][outPort]-1, vALS_TransCav_I['inPorts'][inPort]-1, : ]
    else:
        return vALS_TransCav_I['TFcom'][vALS_TransCav_I['outPorts'][outPort]-1, vALS_TransCav_I['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_TransCav_I_nb = createBaseNoiseBudget(traces, vALS_TransCav_I_TF)


# #### Add extra noise sources
# In this scheme, additional noise comes only due to the residual laser frequency noise in AUX2 laser which is locked to the transfer cavity. There will be additional electronic noise from locking of transfer cavity to PSL as well.
# 
# In the paper, it is measured by directly measuring noise in PDH error signal and assuming it to be dominated by suppressed laser frequency noise.
# 
# Ideally, I think we should use measurement or estimate of free running laser noise for NPRO (maybe as $10^4/f \,\text{Hz}/\sqrt{\text{Hz}}$ from [Optics Letters Vol. 25, Issue 14, pp. 1019-1021 (2000)](https://doi.org/10.1364/OL.25.001019)) injected at AUX2_Freq

# In[ ]:


# Residual Frequency Noise of AUX
vALS_TransCav_I_nb['AUX2 Residual Freq Noise'] = ((1e4 / freq)**2 * np.abs(vALS_TransCav_I_TF('AUX2_Freq'))**2,
                                                  {'label': 'AUX2 Res. Freq.',
                                                   'linewidth': '4', 'linestyle': '-'})


# In[ ]:


style = {'label': 'Transfer Cavity I', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_TransCav_I_nb, style)


# In[ ]:


plotNB(vALS_TransCav_I_nb);


# ---
# ## Transfer Cavity Scheme I with VCO
# 
# <img src="./simulink/vALS_TransCav_I_VCO.svg"/>

# In[ ]:


L = Budget.ifo.Infrastructure.Length
L_MC = 13.54                             # Length of MC
lambda_Seed = Budget.ifo.Laser.Wavelength / 2
lambda_PSL = Budget.ifo.Laser.Wavelength
lambda_AUX = 1550e-9         # AUX wavelength
L_TC = 100e-3                # Length of transfer cavity
FSR_TC = scc.c / (2 * L_TC)
Finesse_TC = 5e4             # Assuming same for both wavelengths
Pole_TC = FSR_TC / ( 2 * Finesse_TC)
Pole_TC_AUX = Pole_TC
mtoHz_TC = scc.c / Budget.ifo.Laser.Wavelength / L_TC
mtoHz_TC_AUX = scc.c / lambda_AUX / L_TC
PZT_FSR_Scan_Voltage = 75
PZT_Act_Slope = FSR_TC / PZT_FSR_Scan_Voltage # Hz/V
PZT_Disp_Slope = Budget.ifo.Laser.Wavelength / PZT_FSR_Scan_Voltage # m/V

# Increase gain as beatnote is taken at longer wavelength
with open('../noiseBudget/ALS_controls.yaml', 'r') as f:
    alsCtrlParams = yaml.full_load(f)
F_ALS = alsCtrlParams['F_ALS']
F_ALS['k'] = F_ALS['k'] * 2.93
F_ALS['z'] = matlab.double([eval(str(el)) for el in F_ALS['z']], is_complex=True)
F_ALS['p'] = matlab.double([eval(str(el)) for el in F_ALS['p']], is_complex=True)
F_ALS['delay'] = float(F_ALS['delay'])

# Transfer Cavity Poles
TC_PSL = {'z': matlab.double([]),
          'p': matlab.double([Pole_TC]),
          'k': Pole_TC
         }
TC_AUX = {'z': matlab.double([]),
          'p': matlab.double([Pole_TC_AUX]),
          'k': Pole_TC_AUX
         }
# Transfer Cavity Locking Feedback Filter
F_TC = {'z': matlab.double([20e3, 20e3, 4e3]),
        'p': matlab.double([1e3, 1e3, 40]),
        'k': 0.5769
       }
F_AUX2 = {'z': matlab.double([1e4, 100, 1]),
          'p': matlab.double([2, 1.2, 1e-4]),
          'k': 0.0504
         }
# Cavity PZT transfer function [m/V]
A_TC_PZT = {'z': matlab.double([]),
            'p': matlab.double([]),
            'k': PZT_Disp_Slope
           }
# Transfer Cavity PDH discriminants
D_AUX_TC = {'z': matlab.double([]),
            'p': matlab.double([]),
            'k': 5e-6
           }
D_PSL_TC = {'z': matlab.double([]),
            'p': matlab.double([]),
            'k': 2.5e-5
           }
# VCO transfer function
A_VCO = {'z': matlab.double([]),
         'p': matlab.double([]),
         'k': 100e3      # Maximum 100kHz/V actuation by Marconi 2023A
        }

# 40m AUX locking filter
F_AUX ={'z': matlab.double([1.0e4, 100, 1]),
        'p': matlab.double([2, 1.2, 1e-4]),
        'k': 0.0504 * 5e6 / A_VCO['k']  # Compensating lost gain due to less actuation
       }
try:
    os.chdir('./simulink/')
    eng = matlab.engine.start_matlab()
    vALS_TransCav_I_VCO = eng.runSimModel('vALS_TransCav_I_VCO', '../../noiseBudget/ALS_controls.yaml',
                                       'mtoHz', scc.c /lambda_PSL / L,
                                       'mtoHz_AUX', scc.c / (lambda_AUX) / L,
                                       'mtoHz_TC', mtoHz_TC,
                                       'mtoHz_TC_AUX', mtoHz_TC_AUX,
                                       'mtoHz_MC', scc.c / lambda_PSL / L_MC,
                                       'Hztom', L * lambda_PSL / scc.c,
                                       'TC_PSL', TC_PSL,
                                       'TC_AUX', TC_AUX,
                                       'F_TC', F_TC,
                                       'F_AUX2', F_AUX2,
                                       'A_TC_PZT', A_TC_PZT,
                                       'D_AUX_TC', D_AUX_TC,
                                       'D_PSL_TC', D_PSL_TC,
                                       'A_VCO', A_VCO,
                                       'F_AUX', F_AUX,
                                       'F_ALS', F_ALS)
    eng.quit()
except BaseException as e:
    print(e)
finally:
    os.chdir(cwd)

for key in ['A', 'B', 'C', 'D']:
    vALS_TransCav_I_VCO[key] = np.asarray(vALS_TransCav_I_VCO[key])

vALS_TransCav_I_VCO['ss'] = control.ss(vALS_TransCav_I_VCO['A'], vALS_TransCav_I_VCO['B'], vALS_TransCav_I_VCO['C'], vALS_TransCav_I_VCO['D'])
vALS_TransCav_I_VCO['TFmag'], vALS_TransCav_I_VCO['TFph'], _ = control.freqresp(vALS_TransCav_I_VCO['ss'], 2 * np.pi * freq)
vALS_TransCav_I_VCO['TFcom'] = vALS_TransCav_I_VCO['TFmag'] * np.exp(1j * vALS_TransCav_I_VCO['TFph'])
    
def vALS_TransCav_I_VCO_TF(inPort, outPort='Res_Disp', ff=None):
    if ff is not None:
        TFmag, TFph, _ = control.freqresp(vALS_TransCav_I_VCO['ss'], 2 * np.pi * ff)
        TFcom = TFmag * np.exp(1j * TFph)
        return TFcom[vALS_TransCav_I_VCO['outPorts'][outPort]-1, vALS_TransCav_I_VCO['inPorts'][inPort]-1, : ]
    else:
        return vALS_TransCav_I_VCO['TFcom'][vALS_TransCav_I_VCO['outPorts'][outPort]-1, vALS_TransCav_I_VCO['inPorts'][inPort]-1, : ]


# #### Create bease noise budget with common noises

# In[ ]:


vALS_TransCav_I_VCO_nb = createBaseNoiseBudget(traces, vALS_TransCav_I_VCO_TF)


# #### Add extra noise sources
# In this scheme, additional noise comes due to the residual laser frequency noise in AUX2 laser which is locked to the transfer cavity. There will be additional electronic noise from locking of transfer cavity to PSL as well.
# 
# In the paper, it is measured by directly measuring noise in PDH error signal and assuming it to be dominated by suppressed laser frequency noise.
# 
# Ideally, I think we should use measurement or estimate of free running laser noise for NPRO (maybe as $10^4/f \,\text{Hz}/\sqrt{\text{Hz}}$ from [Optics Letters Vol. 25, Issue 14, pp. 1019-1021 (2000)](https://doi.org/10.1364/OL.25.001019)) injected at AUX2_Freq
# 
# AUX is generated by picking off this reference AUX2 beam, upshifting it with an AOM and controlling the frequency through a VCO. The VCO would add frequency noise at AUX_VCO.
# 
# A measurement of frequency noise of Marconi 2023A was made in this [elog](https://nodus.ligo.caltech.edu:8081/CTN/2286) at different actuation slopes. Taking worst case scenario for high actuaiton slope of 100 kHz/V, $100 mHz/\sqrt{Hz}$ flat noise can be assumed. This is what needs to be minimized. If the 40m can be locked with lower actuation slope, it would be nice.

# In[ ]:


# Residual Frequency Noise of AUX
vALS_TransCav_I_VCO_nb['AUX2 Residual Freq Noise'] = ((1e4 / freq)**2 * np.abs(vALS_TransCav_I_VCO_TF('AUX2_Freq'))**2,
                                                      {'label': 'AUX2 Res. Freq.',
                                                      'linewidth': '4', 'linestyle': '-'})
# VCO Noise
vALS_TransCav_I_VCO_nb['VCO AUX'] = ((0.1 * np.abs(vALS_TransCav_I_VCO_TF('AUX_VCO')))**2,
                                     {'label': 'VCO AUX', 'linewidth': '4', 'linestyle': '--'})


# In[ ]:


style = {'label': 'Transfer Cavity I with VCO', 'linewidth': '4', 'linestyle': '-'}
calcTotalNoise(vALS_TransCav_I_VCO_nb, style)


# In[ ]:


plotNB(vALS_TransCav_I_VCO_nb);


# ## Compare total noises of different schemes

# In[ ]:


schemes = [vALS_FHG_I_nb, vALS_FHG_Ib_nb, vALS_FHG_II_nb,
           vALS_SROPO_Ib_nb, vALS_SROPO_I_nb, vALS_SROPO_II_nb,
           vALS_FreqComb_IIb_nb,
           vALS_PSROPO_I_nb, vALS_PSROPO_II_nb,
           vALS_TransCav_I_nb, vALS_TransCav_I_VCO_nb]

fig, ax = plt.subplots(1,1, figsize=(14,9))

# Title
ax.set_title('Mariner ALS Scheme Comparison ')

# Noise curves
for sch in schemes:
    trace = sch['Total'][0]**.5
    style = sch['Total'][1]
    ax.plot(freq, trace, **style)
    
# Plot formatting
ax.set_xlim([freq[0], freq[-1]])
#ax.set_ylim([1e-20, 1e-5])
ax.set_xlabel(u"Frequency [Hz]")
ax.set_ylabel(u"Displacement Noise ASD [m/\u221AHz]")
ax.set_xscale('log')
ax.set_yscale('log')
ax.legend(ncol=3);
ax.tick_params(right=False)
secaxy = ax.secondary_yaxis('right', functions=(mtoHz, Hztom))
secaxy.set_ylabel(u"Frequency Noise ASD [Hz/\u221AHz]")

# Uncomment to save figure
fig.savefig('vALS_Sch_Comp.pdf', bbox_inches='tight')

