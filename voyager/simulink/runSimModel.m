function retStruc = runSimModel(model, yamlFile, varargin)
%
% Function to load simulink model and return a structure with signal names
% and corresponding port numbers of all inports and outports and complete
% systems' state-space A,B,C,D
% yamlFile can be used to upload subSystem zpk models. If empty string is
% provided, it will be ignored.
% Designed to be run from python.

% All extra arguments after yamlFile are assumed to be name-value pairs
% These would be made available in base workspace. This is for possible
% use by simulink model

retStruc = mapPorts(model);

if yamlFile ~= ""
    ctrl = yaml.ReadYaml(yamlFile);
    subSys = fieldnames(ctrl);
    for k=1:numel(subSys)
        zpkFields = fieldnames(ctrl.(subSys{k}));
        if ~any(strcmp(zpkFields, 'z'))
            ctrl.(subSys{k}).z = [];
        else
            tmp=[];
            for n=1:numel(ctrl.(subSys{k}).z)
                value = ctrl.(subSys{k}).z(n);
                if iscell(value)
                    value = cell2mat(value);
                end
                if ischar(value)
                    value = eval(value);
                end
                tmp(n) = 2*pi*value;
            end
            ctrl.(subSys{k}).z = tmp;
        end
        if ~any(strcmp(zpkFields, 'p'))
            ctrl.(subSys{k}).p = [];
        else
            tmp=[];
            for n=1:numel(ctrl.(subSys{k}).p)
                value = ctrl.(subSys{k}).p(n);
                if iscell(value)
                    value = cell2mat(value);
                end
                if ischar(value)
                    value = eval(value);
                end
                tmp(n) = 2*pi*value;
            end
            ctrl.(subSys{k}).p = tmp;
        end
        if any(strcmp(zpkFields, 'delay'))
            [num, den] = pade(ctrl.(subSys{k}).delay, 4);
            ctrl.(subSys{k}).delay = tf(num,den);
        end
        ctrl.(subSys{k}).sys = zpk(-ctrl.(subSys{k}).z,...
            -ctrl.(subSys{k}).p,...
            ctrl.(subSys{k}).k);
        assignin('base', subSys{k}, ctrl.(subSys{k}))
    end
end

for k=1:numel(varargin)/2
    if isstruct(varargin{2*k})
        if (isfield(varargin{2*k}, 'z') && ...
            isfield(varargin{2*k}, 'p') && ...
            isfield(varargin{2*k}, 'k'))
            varargin{2*k}.sys = zpk(-2*pi*varargin{2*k}.z, ...
                                    -2*pi*varargin{2*k}.p, ...
                                    varargin{2*k}.k);
        end
        if isfield(varargin{2*k}, 'delay')
            [num, den] = pade(varargin{2*k}.delay, 4);
            varargin{2*k}.delay = tf(num,den);
        end
    end
    assignin('base', varargin{2*k-1}, varargin{2*k})
end

% --------------------------------------------------------
% Makes the State-Space object from the .mdl

[retStruc.A, retStruc.B, retStruc.C, retStruc.D] = linmod2(model);

end
