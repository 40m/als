# ALS Noise Budget

This folder should contain code to calculate and plot noise budget of ALS
with latest measured data.

---
## Noise Budget Plot

![ALS_nb](ALS_nb.png)

**[ALS_nb.pdf](https://git.ligo.org/40m/als/-/blob/master/noiseBudget/ALS_nb.pdf)**

Latest elog reporting noise budget:
* [08/17/2020: ALS noise improvement with whitening](https://nodus.ligo.caltech.edu:8081/40m/15531)
* [02/14/2020: Slow Common Mode ALS Noise Budget and OLTF](https://nodus.ligo.caltech.edu:8081/40m/15212)
* [01/26/2020: IR ALS noise measurement](https://nodus.ligo.caltech.edu:8081/40m/15157)
* [08/15/2019: Sensing noise in IR ALS due to pick-off before IMC, investigation](https://nodus.ligo.caltech.edu:8081/40m/14846)
* [07/24/2019: ALS Single Arm Locked through ALS, POX noise measurement](https://nodus.ligo.caltech.edu:8081/40m/14802)
* [04/09/2019: ALS noise budget](https://nodus.ligo.caltech.edu:8081/40m/14528)
* [04/12/2018: ALS noise budget](https://nodus.ligo.caltech.edu:8081/40m/13749)


---
## Notes:
"DFD noise" is the quadrature sum of the following terms:

* ZHL-3A (RF amplifier) noise, NF ~ 6dB per spec (~ 1nV/rtHz)
* Delay line demod board noise, ~30nV/rtHz [measurement]
* AA board noise [measurement]
* ADC noise

Currently, **ADC** noise is suspected to be the dominant noise source.
