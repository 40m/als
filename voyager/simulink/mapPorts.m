function portMap = mapPorts(model)
    load_system(model)
    blks = find_system(model,'Type','block');
    N = get_param(blks, 'BlockType');
    j = 1;
    k = 1;
    for i = 1:length(N)
        if ( strcmp(N{i},'Inport'))
            in{j} = blks{i};
            j = j + 1;
        elseif ( strcmp(N{i}, 'Outport'))
            out{k} = blks{i};
            k = k + 1;
        end
    end
    for i = 1:length(in)
        p = get_param(in{i}, 'PortHandles');
        sigName = get_param(get_param(p.Outport, 'Line'), 'Name');
        portMap.inPorts.(sigName) = uint64(i);
    end
    for i = 1:length(out)
        p = get_param(out{i}, 'PortHandles');
        sigName = get_param(get_param(p.Inport, 'Line'), 'Name');
        portMap.outPorts.(sigName) = uint64(i);
    end
end